﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskerApp
{
    public partial class Tasker : MetroForm
    {
        private DataManager dataManager;

        public Tasker()
        {
            InitializeComponent();
            dataManager = new DataManager(metroGrid1);
            metroGrid1.Font = new Font("Segoe UI", 11f, FontStyle.Regular, GraphicsUnit.Pixel);
            DateTimePicker.Value = DateTime.Today;
            metroGrid1.BorderStyle = (BorderStyle)MetroFormBorderStyle.FixedSingle;
            this.AcceptButton = this.Add;
            this.DBPath_tbox.Text = Settings.FilePath;
            this.days_tbox.Text = Settings.DaysToStore.ToString();
            this.saveSettings_btn.Enabled = false;
            this.cancelSettings_btn.Enabled = false;
        }

        private void UpdateGrid(object sender, EventArgs e)
        {
            dataManager.UpdateView(DateTimePicker.Value);
        }

        private void Add_Click(object sender, EventArgs e)
        {
            Add.Enabled = false;
            Add_Form add = new Add_Form(dataManager, DateTimePicker.Value);
            add.ShowDialog();
            Add.Enabled = true;
            Add.Focus();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count != 0)
            {
                dataManager.Delete(metroGrid1.SelectedRows[0], DateTimePicker.Value);
            }
        }

        private void saveSettings_btn_Click(object sender, EventArgs e)
        {
            uint days;
            
            try
            {
                days = uint.Parse(days_tbox.Text);
            }
            catch
            {
                days = 0;
            }
            
            Settings.ChangeSettings(DBPath_tbox.Text.Trim(), days);
            this.dataManager.Save();
            this.cancelSettings_btn_Click(sender, e);
        }

        private void cancelSettings_btn_Click(object sender, EventArgs e)
        {
            DBPath_tbox.Text = Settings.FilePath;
            days_tbox.Text = Settings.DaysToStore.ToString();
            this.DisableSettingsButtons();
        }

        private void EnableSettingsButtons(object sender, EventArgs e)
        {
            saveSettings_btn.Enabled = true;
            cancelSettings_btn.Enabled = true;
        }

        private void DisableSettingsButtons()
        {
            saveSettings_btn.Enabled = false;
            cancelSettings_btn.Enabled = false;
        }

        private void DBFolder_btn_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = Settings.FilePath;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.DBPath_tbox.Text = saveFileDialog1.FileName;
            }
        }
    }
}
