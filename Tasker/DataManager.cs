﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace TaskerApp
{
    public class DataManager
    {
        public DataTable Data { get; private set; }
        private DataGridView Grid { get; set; }

        public DataManager(DataGridView grid)
        {
            Data = new DataTable("Tasks base");
            Grid = grid;
            DataColumn column;

            if (File.Exists(Settings.FilePath))
            {
                Data.ReadXml(Settings.FilePath);

                if (Settings.DaysToStore > 0)
                {
                    this.CleanUp();
                }
            }
            else
            {
                this.CheckDir();

                column = new DataColumn("id", typeof(uint));
                Data.Columns.Add(column);

                column = new DataColumn("message", typeof(string));
                Data.Columns.Add(column);

                column = new DataColumn("date", typeof(string));
                Data.Columns.Add(column);

                Data.AcceptChanges();
            }

            Grid.DataSource = Data;

            Grid.Columns[0].Visible = false;
            Grid.Columns[2].Visible = false;
        }

        public void Add(string message, DateTime date)
        {
            DataRow row = Data.NewRow();

            //row["id"] = 1;
            row["message"] = message;
            row["date"] = date.ToString();

            Data.Rows.Add(row);
            Data.AcceptChanges();

            this.CheckDir();

            Data.WriteXml(Settings.FilePath, XmlWriteMode.WriteSchema);
            this.UpdateView(date);
        }

        private void CleanUp()
        {
            List<DataRow> rowsToDelete = new List<DataRow>();

            foreach (DataRow row in Data.Rows)
            {
                if (DateTime.Parse(row["date"].ToString()) < DateTime.Today.AddDays(-(Settings.DaysToStore)))
                {
                    rowsToDelete.Add(row);
                }
            }

            foreach (DataRow row in rowsToDelete)
            {
                Data.Rows.Remove(row);
            }

            Data.AcceptChanges();
            Data.WriteXml(Settings.FilePath, XmlWriteMode.WriteSchema);
        }

        public void Delete(DataGridViewRow selectedRow, DateTime date)
        {
            foreach (DataRow row in Data.Rows)
            {
                if (row["date"].ToString() == date.ToString() && selectedRow.Cells["message"].Value.ToString() == row["message"].ToString())
                {
                    row.Delete();
                    break;
                }
            }
            Data.AcceptChanges();

            this.CheckDir();

            Data.WriteXml(Settings.FilePath, XmlWriteMode.WriteSchema);
            this.UpdateView(date);
        }

        public void UpdateView(DateTime date)
        {
            Grid.CurrentCell = null;
            foreach (DataGridViewRow row in Grid.Rows)
            {
                if (row.Cells["date"].Value.ToString() == date.ToString())
                {
                    row.Visible = true;
                }
                else
                {
                    row.Visible = false;
                }
            }
        }

        private void CheckDir()
        {
            string dir = Path.GetDirectoryName(Settings.FilePath);

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public void Save()
        {
            Data.WriteXml(Settings.FilePath, XmlWriteMode.WriteSchema);
        }
    }
}
