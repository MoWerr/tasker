﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace TaskerApp
{
    public partial class Add_Form : MetroForm
    {
        private DataManager dataManager;
        private DateTime pickedDate;

        public Add_Form(DataManager data, DateTime date)
        {
            InitializeComponent();
            dataManager = data;
            pickedDate = date;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            dataManager.Add(metroTextBox1.Text.ToString(), pickedDate);
            this.Close();
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetFocus(object sender, EventArgs e)
        {
            this.metroTextBox1.Focus();
        }
    }
}
