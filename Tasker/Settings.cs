﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

namespace TaskerApp
{
    public static class Settings
    {
        public static string FilePath { get; set; }
        public static uint DaysToStore { get; set; }
        private static string SettingsPath { get; set; }
        private static DataTable SettingsTable { get; set; }

        static Settings()
        {
            SettingsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\MoWerr apps\Tasker\Data\settings.xml";
            SettingsTable = new DataTable("Settings");

            if (File.Exists(SettingsPath))
            {
                SettingsTable.ReadXml(SettingsPath);
            }
            else
            {
                string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\MoWerr apps\Tasker\Data";

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                DataColumn column = new DataColumn("FilePath", typeof(string));
                SettingsTable.Columns.Add(column);

                column = new DataColumn("Days", typeof(uint));
                SettingsTable.Columns.Add(column);

                DataRow row = SettingsTable.NewRow();
                row["FilePath"] = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\MoWerr apps\Tasker\Data\tasks_DB.xml";
                row["Days"] = 7;
                SettingsTable.Rows.Add(row);

                SettingsTable.AcceptChanges();
                SettingsTable.WriteXml(SettingsPath, XmlWriteMode.WriteSchema);
            }

            FilePath = SettingsTable.Rows[0]["FilePath"].ToString();
            DaysToStore = (uint)SettingsTable.Rows[0]["Days"];
        }

        public static void ChangeSettings(string path, uint days)
        {
            DataRow row = SettingsTable.NewRow();
            row["FilePath"] = path;
            row["Days"] = days;

            SettingsTable.Rows[0].Delete();
            SettingsTable.Rows.Add(row);

            SettingsTable.AcceptChanges();
            SettingsTable.WriteXml(SettingsPath, XmlWriteMode.WriteSchema);

            FilePath = SettingsTable.Rows[0]["FilePath"].ToString();
            DaysToStore = (uint)SettingsTable.Rows[0]["Days"];

            if (!File.Exists(FilePath))
            {
                string dir = Path.GetDirectoryName(FilePath);

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
        }
    }
}
